FROM php:7.4-apache

RUN apt-get update && \
    apt-get install -y --no-install-recommends git zip

RUN apt-get install -y sendmail libpng-dev

RUN apt-get install -y zlib1g-dev
RUN apt-get install -y libxslt-dev
RUN apt-get install -y graphviz
RUN apt-get install -y libssl-dev

RUN docker-php-ext-install mysqli
RUN docker-php-ext-install pdo pdo_mysql
RUN docker-php-ext-install gd
RUN docker-php-ext-install xsl
RUN docker-php-ext-install intl

RUN curl --silent --show-error https://getcomposer.org/installer | php

# Copy project to container
COPY . /var/www/html

# Setting chmod
RUN cd /var/www/html/ && \
    chmod -R 777 storage/logs/ && \
    chmod -R 777 storage/framework/views/ && \
    chmod -R 777 storage/framework/cache/

RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/ssl-cert-snakeoil.key -out /etc/ssl/certs/ssl-cert-snakeoil.pem -subj "/C=AT/ST=Vienna/L=Vienna/O=Security/OU=Development/CN=example.com"

# Apache config
RUN a2enmod rewrite
RUN a2enmod vhost_alias
RUN a2ensite default-ssl
RUN a2enmod ssl

EXPOSE 80
EXPOSE 443
