<?php

namespace App\Exceptions;

use Exception;

class InvalidTransactionException extends Exception
{
    protected $code = 404;
    protected $message = 'TRANSACTION.INVALID_TRANSACTION';
}
