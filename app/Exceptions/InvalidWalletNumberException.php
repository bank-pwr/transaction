<?php

namespace App\Exceptions;

use Exception;

class InvalidWalletNumberException extends Exception
{
    protected $code = 404;
    protected $message = 'TRANSACTION.WALLET.INVALID_NUMBER';
}
