<?php

namespace App\Exceptions;

use Exception;

class NotEnoughMoneyException extends Exception
{
    protected $code = 419;
    protected $message = 'TRANSACTION.WALLET.NOT_ENOUGH_MONEY';
}
