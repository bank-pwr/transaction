<?php

namespace App\Exceptions;

use Exception;

class WalletBlockedException extends Exception
{
    protected $code = 419;
    protected $message = 'TRANSACTION.WALLET.BLOCKED';
}
