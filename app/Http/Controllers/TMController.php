<?php

namespace App\Http\Controllers;

use App\Http\Requests\MakeTransactionRequest;
use App\Http\Requests\TransactionRequest;
use App\RequestValidators\RequestValidator;
use Illuminate\Http\Request;
use TM\Facades\TM;
use TM\Models\CashTransactions;

class TMController extends Controller
{
    public function getAllTransactions() {
        try {
            $transactions = TM::getAllTransactions();

            return $this->response(true, 200, [
                'transactions' => $transactions
            ]);
        } catch (\Exception $exception) {
            return $this->response(false, 419, ['message' => $exception->getMessage()]);
        }
    }

    public function makeTransaction(Request $request) {
        try {
            RequestValidator::validate($request, new MakeTransactionRequest());
            TM::makeTransaction($request->from_wallet_number, $request->to_wallet_number, $request->amount);

            return $this->response(true, 200);
        } catch (\Exception $exception) {
            return $this->response(false, 419, ['message' => $exception->getMessage()]);
        }
    }

    public function acceptTransaction(Request $request) {
        try {
            RequestValidator::validate($request, new TransactionRequest());
            TM::acceptTransaction($request->transaction_id);
            $transaction = CashTransactions::with(CashTransactions::WITH_ACCEPTED_WALLET)
                ->find($request->transaction_id);

            return $this->response(true, 200, ['transaction' => $transaction]);
        } catch (\Exception $exception) {
            return $this->response(false, 419, ['message' => $exception->getMessage()]);
        }
    }

    public function rollbackTransaction(Request $request) {
        try {
            RequestValidator::validate($request, new TransactionRequest());
            TM::rollbackTransaction($request->transaction_id);

            return $this->response(true, 200);
        } catch (\Exception $exception) {
            return $this->response(false, 419, ['message' => $exception->getMessage()]);
        }
    }
}
