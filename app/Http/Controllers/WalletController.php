<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlockWalletRequest;
use App\Http\Requests\CreateWalletRequest;
use App\Http\Requests\UserRequest;
use App\RequestValidators\RequestValidator;
use App\Services\WalletService;
use Illuminate\Http\Request;
use TM\Models\Wallets;

class WalletController extends Controller
{
    protected WalletService $walletService;

    public function __construct(WalletService $walletService) {
        $this->walletService = $walletService;
    }

    public function createWallet(Request $request) {
        try {
            RequestValidator::validate($request, new CreateWalletRequest());
            $this->walletService->createWallet($request->user_id);

            return $this->response(true, 200);
        } catch (\Exception $exception) {
            return $this->response(false, 419, ['message' => $exception->getMessage()]);
        }
    }

    public function getUserInfo(Request $request) {
        try {
            RequestValidator::validate($request, new UserRequest());
            $wallet = $this->walletService->findWalletByUserId($request->user_id);

            return $this->response(true, 200, ['wallet' => $wallet]);
        } catch (\Exception $exception) {
            return $this->response(false, 419, ['message' => $exception->getMessage()]);
        }
    }

    public function getUserTransactions(Request $request) {
        try {
            RequestValidator::validate($request, new UserRequest());
            $wallet = $this->walletService->findWalletTransactionsByUserId($request->user_id);

            return $this->response(true, 200, [
                'inbound_transactions' => $wallet->{Wallets::WITH_INBOUND_TRANSACTIONS} ?? [],
                'outgoing_transactions' => $wallet->{Wallets::WITH_OUTGOING_TRANSACTIONS} ?? [],
            ]);
        } catch (\Exception $exception) {
            return $this->response(false, 419, ['message' => $exception->getMessage()]);
        }
    }

    public function getWallets() {
        try {
            $wallets = $this->walletService->getWallets();

            return $this->response(true, 200, [
                'wallets' => $wallets
            ]);
        } catch (\Exception $exception) {
            return $this->response(false, 419, ['message' => $exception->getMessage()]);
        }
    }

    public function lockWallet(Request $request) {
        try {
            RequestValidator::validate($request, new BlockWalletRequest());
            $this->walletService->lockWallet($request->wallet_id);

            return $this->response(true, 200);
        } catch (\Exception $exception) {
            return $this->response(false, 419, ['message' => $exception->getMessage()]);
        }
    }

    public function unlockWallet(Request $request) {
        try {
            RequestValidator::validate($request, new BlockWalletRequest());
            $this->walletService->unlockWallet($request->wallet_id);

            return $this->response(true, 200);
        } catch (\Exception $exception) {
            return $this->response(false, 419, ['message' => $exception->getMessage()]);
        }
    }
}
