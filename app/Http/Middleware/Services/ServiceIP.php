<?php

namespace App\Http\Middleware\Services;

use Closure;
use Illuminate\Http\Request;

class ServiceIP
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $ip  = env('GATEWAY_IP', null);

        if (is_null($ip) || $request->ip() !== $ip) {
            return response(['message' => 'Forbidden! Bad IP'], 403);
        }
        return $next($request);
    }
}
