<?php

namespace App\Http\Middleware\Services;

use Closure;
use Illuminate\Http\Request;

class ServiceKey
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $moduleKey = $request->header('Module-key');
        $key  = env('API_KEY', null);

        if (is_null($key) || $key !== $moduleKey) {
            return response(['message' => 'Forbidden! Bad key'], 403);
        }
        return $next($request);
    }
}
