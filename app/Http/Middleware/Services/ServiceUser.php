<?php

namespace App\Http\Middleware\Services;

use Closure;
use Illuminate\Http\Request;

class ServiceUser
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->get('user');

        if (!is_array($user) || !array_key_exists('id', $user)) {
            return response(['message' => 'Forbidden! Bad User'], 403);
        }

        return $next($request);
    }
}
