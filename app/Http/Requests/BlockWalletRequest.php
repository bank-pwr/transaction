<?php

namespace App\Http\Requests;

use App\Contracts\Request;
use TM\Models\Wallets;

class BlockWalletRequest implements Request
{

    public function rules(): array
    {
        return [
            'wallet_id' => 'required|numeric',
        ];
    }

    public function messages(): array
    {
        return [

        ];
    }
}
