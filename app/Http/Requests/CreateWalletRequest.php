<?php

namespace App\Http\Requests;

use App\Contracts\Request;
use TM\Models\Wallets;

class CreateWalletRequest implements Request
{

    public function rules(): array
    {
        return [
            'user_id' => 'required|numeric|unique:'.Wallets::TABLE,
        ];
    }

    public function messages(): array
    {
        return [

        ];
    }
}
