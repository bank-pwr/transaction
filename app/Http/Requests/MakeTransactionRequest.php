<?php

namespace App\Http\Requests;

use App\Contracts\Request;
use TM\Models\Wallets;

class MakeTransactionRequest implements Request
{

    public function rules(): array
    {
        return [
            'from_wallet_number' => 'required|numeric',
            'to_wallet_number' => 'required|numeric',
            'amount' => 'required|numeric',
        ];
    }

    public function messages(): array
    {
        return [

        ];
    }
}
