<?php

namespace App\Http\Requests;

use App\Contracts\Request;

class UserRequest implements Request
{

    public function rules(): array
    {
        return [
            'user_id' => 'required|numeric',
        ];
    }

    public function messages(): array
    {
        return [

        ];
    }
}
