<?php

namespace App\RequestValidators;

use Illuminate\Http\Request;
use App\Contracts\Request as RequestContract;
use Illuminate\Support\Facades\Validator;

class RequestValidator
{
    public static function validate(Request $request, RequestContract $validator): void {
        Validator::make($request->toArray(), $validator->rules(), $validator->messages())->validate();
    }

    public static function isValid(Request $request, RequestContract $validator): bool {
        try {
            self::validate($request, $validator);

            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }
}
