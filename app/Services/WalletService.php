<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use TM\Models\CashTransactions;
use TM\Models\Wallets;

class WalletService
{
    protected Wallets $wallets;
    protected CashTransactions $transactions;

    public function __construct(Wallets $wallets, CashTransactions $transactions) {
        $this->wallets = $wallets;
        $this->transactions = $transactions;
    }

    public function createWallet(int $userId): Wallets {
        return $this->wallets->create([
            Wallets::USER_ID => $userId,
            Wallets::AMOUNT => 1000,
            Wallets::BLOCKED => false,
            Wallets::WALLET_NUMBER => $this->generateRandomNumber(20),
        ]);
    }

    public function findWalletByUserId(int $userId): ?Model {
        return  $this->wallets
            ->where(Wallets::USER_ID, $userId)->first();
    }

    public function findWalletTransactionsByUserId(int $userId): ?Model {
        return  $this->wallets
            ->with([
                Wallets::WITH_INBOUND_TRANSACTIONS.'.'.CashTransactions::WITH_FROM_WALLET,
                Wallets::WITH_INBOUND_TRANSACTIONS => function(HasMany $query) {
                    $query->where(CashTransactions::STATUS, CashTransactions::STATUS_ACCEPTED)
                        ->orderBy(CashTransactions::CREATED_AT, 'DESC');
                },
                Wallets::WITH_OUTGOING_TRANSACTIONS.'.'.CashTransactions::WITH_TO_WALLET,
                Wallets::WITH_OUTGOING_TRANSACTIONS => function(HasMany $query) {
                    $query->orderBy(CashTransactions::CREATED_AT, 'DESC');
                },
            ])
            ->where(Wallets::USER_ID, $userId)->first();
    }

    public function lockWallet(int $walletId): void {
        $wallet = $this->wallets->findOrFail($walletId);

        $wallet->{Wallets::BLOCKED} = 1;
        $wallet->save();
    }

    public function unlockWallet(int $walletId): void {
        $wallet = $this->wallets->findOrFail($walletId);

        $wallet->{Wallets::BLOCKED} = 0;
        $wallet->save();
    }

    public function getWallets(): Collection {
        return $this->wallets->all();
    }

    private function generateRandomNumber($length = 10): string {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
