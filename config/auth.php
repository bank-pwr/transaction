<?php

return [
    'cors' => [
        'supportsCredentials' => false,
        'allowedOrigins' => ['*'],
        'allowedHeaders' => ['Content-Type', 'X-Requested-With', 'Authorizations', 'Origin', 'Accept', 'token', 'Authorization', 'X-Auth-Token'],
        'allowedMethods' => ['*'], // ex: ['GET', 'POST', 'PUT',  'DELETE']
    ],
];
