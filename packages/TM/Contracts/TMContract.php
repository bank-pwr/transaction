<?php

namespace TM\Contracts;

use Illuminate\Database\Eloquent\Collection;
use TM\Models\CashTransactions;

interface TMContract
{
    public function getAllTransactions(): Collection;

    public function makeTransaction(string $fromWalletNumber, string $toWalletNumber, float $amount): CashTransactions;

    public function rollbackTransaction(int $transactionId): void;

    public function acceptTransaction(int $transactionId): void;
}
