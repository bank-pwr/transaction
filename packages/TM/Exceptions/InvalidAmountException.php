<?php

namespace TM\Exceptions;

use Exception;

class InvalidAmountException extends Exception
{
    protected $code = 403;
    protected $message = "Amount has to be natural number.";
}
