<?php

namespace TM\Exceptions;

use Exception;

class InvalidValueException extends Exception
{
    protected $code = 403;
    protected $message = "Invalid value!";
}
