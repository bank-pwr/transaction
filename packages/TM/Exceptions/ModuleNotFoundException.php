<?php

namespace TM\Exceptions;

use Exception;

class ModuleNotFoundException extends Exception
{
    protected $code = 404;
    protected $message = "Method module(moduleName) not used or module doesn't exist!";
}
