<?php

namespace TM\Exceptions;

use Exception;

class UserNotFoundException extends Exception
{
    protected $code = 404;
    protected $message = "Method user(userId) not used or user doesn't exist!";
}
