<?php

namespace TM\Exceptions;

use Exception;

class WalletNotFoundException extends Exception
{
    protected $code = 404;
    protected $message = "Wallet not found";
}
