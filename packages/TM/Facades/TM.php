<?php

namespace TM\Facades;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Facade;
use TM\Models\CashTransactions;

/**
 * @method static Collection getAllTransactions()
 * @method static CashTransactions makeTransaction(string $fromWalletNumber, string $toWalletNumber, float $amount)
 * @method static void rollbackTransaction(int $transactionId)
 * @method static void acceptTransaction(int $transactionId)
 */
class TM extends Facade
{
    protected static function getFacadeAccessor() {
        return static::class;
    }
}
