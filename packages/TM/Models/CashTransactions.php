<?php

namespace TM\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CashTransactions extends Model
{
    const TABLE = 'tm_cash_transactions';
    const ID = 'id';
    const FROM_WALLET_ID = 'from_wallet_id';
    const TO_WALLET_ID = 'to_wallet_id';
    const VALUE = 'value';
    const STATUS = 'status';

    const WITH_FROM_WALLET = 'fromWallet';
    const WITH_TO_WALLET = 'toWallet';
    const WITH_ACCEPTED_WALLET = 'acceptedWallet';

    const STATUS_PENDING = 0;
    const STATUS_ACCEPTED = 1;
    const STATUS_ROLL_BACK = 2;

    protected $table = self::TABLE;
    protected $primaryKey = self::ID;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::ID, self::FROM_WALLET_ID, self::TO_WALLET_ID,
        self::VALUE, self::STATUS
    ];

    public function fromWallet(): BelongsTo {
        return  $this->belongsTo(Wallets::class, self::FROM_WALLET_ID, Wallets::ID)
            ->select([Wallets::ID, Wallets::WALLET_NUMBER]);
    }

    public function toWallet(): BelongsTo {
        return  $this->belongsTo(Wallets::class, self::TO_WALLET_ID, Wallets::ID)
            ->select([Wallets::ID, Wallets::WALLET_NUMBER]);
    }

    public function acceptedWallet(): BelongsTo {
        return  $this->belongsTo(Wallets::class, self::TO_WALLET_ID, Wallets::ID)
            ->select([Wallets::ID, Wallets::WALLET_NUMBER, Wallets::USER_ID]);
    }
}
