<?php

namespace TM\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Wallets extends Model
{
    const TABLE = 'tm_wallets';
    const ID = 'id';
    const USER_ID = 'user_id';
    const WALLET_NUMBER = 'wallet_number';
    const AMOUNT = 'amount';
    const BLOCKED = 'blocked';

    const WITH_OUTGOING_TRANSACTIONS = 'outgoingTransactions';
    const WITH_INBOUND_TRANSACTIONS = 'inboundTransactions';

    protected $table = self::TABLE;
    protected $primaryKey = self::ID;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::ID, self::USER_ID, self::WALLET_NUMBER, self::AMOUNT,
        self::BLOCKED
    ];

    public function outgoingTransactions():HasMany {
        return $this->hasMany(CashTransactions::class, CashTransactions::FROM_WALLET_ID, self::ID);
    }

    public function inboundTransactions():HasMany {
        return $this->hasMany(CashTransactions::class, CashTransactions::TO_WALLET_ID, self::ID);
    }
}
