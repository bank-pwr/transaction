<?php

namespace TM\Providers;

use Illuminate\Support\ServiceProvider;
use TM\Facades\TM;
use TM\Services\TMService;

class TMServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadMigrationsFrom(base_path().'/packages/TM/database/migrations');

        $this->app->bind(TM::class, TMService::class);
    }

    public function boot() {
        //
    }
}
