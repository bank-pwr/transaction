<?php

namespace TM\Services;

use App\Exceptions\InvalidTransactionException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use TM\Contracts\TMContract;
use TM\Exceptions\InvalidAmountException;
use TM\Exceptions\WalletNotFoundException;
use TM\Models\CashTransactions;
use TM\Models\Wallets;

final class TMService implements TMContract
{
    protected Wallets $wallets;
    protected CashTransactions $transactions;

    public function __construct(Wallets $wallets, CashTransactions $transactions) {
        $this->wallets = $wallets;
        $this->transactions = $transactions;
    }

    public function getAllTransactions(): Collection {
        return $this->transactions
            ->with([CashTransactions::WITH_TO_WALLET, CashTransactions::WITH_FROM_WALLET])
            ->orderBy(CashTransactions::CREATED_AT, 'DESC')->get();
    }

    /**
     * @param string $fromWalletNumber
     * @param string $toWalletNumber
     * @param float $amount
     * @return CashTransactions
     * @throws InvalidAmountException
     * @throws WalletNotFoundException
     * @throws \Exception
     */
    public function makeTransaction(string $fromWalletNumber, string $toWalletNumber, float $amount): CashTransactions {
        $fromWallet = $this->findWalletByNumber($fromWalletNumber, true);
        $toWallet = $this->findWalletByNumber($toWalletNumber);

        if ($fromWallet->{Wallets::AMOUNT} < $amount) {
            throw new InvalidAmountException();
        }

        try {
            DB::beginTransaction();
            $transaction = $this->saveTransaction($fromWallet->{Wallets::ID}, $toWallet->{Wallets::ID}, $amount);
            $this->subtractFromWallet($fromWallet->{Wallets::ID}, $amount);
            DB::commit();

            return $transaction;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @param int $transactionId
     * @throws InvalidTransactionException
     * @throws \Exception
     */
    public function rollbackTransaction(int $transactionId): void {
        $transaction = $this->findTransaction($transactionId);
        try {
            DB::beginTransaction();
            $transaction->{CashTransactions::STATUS} = CashTransactions::STATUS_ROLL_BACK;
            $transaction->save();
            $this->addToWallet($transaction->{CashTransactions::FROM_WALLET_ID}, $transaction->{CashTransactions::VALUE});
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @param int $transactionId
     * @throws InvalidTransactionException
     * @throws \Exception
     */
    public function acceptTransaction(int $transactionId): void {
        $transaction = $this->findTransaction($transactionId);

        try {
            DB::beginTransaction();
            $transaction->{CashTransactions::STATUS} = CashTransactions::STATUS_ACCEPTED;
            $transaction->save();
            $this->addToWallet($transaction->{CashTransactions::TO_WALLET_ID}, $transaction->{CashTransactions::VALUE});
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    private function saveTransaction(int $fromWalletId, int $toWalletId, float $amount): CashTransactions {
        return $this->transactions->create([
            CashTransactions::FROM_WALLET_ID => $fromWalletId,
            CashTransactions::TO_WALLET_ID => $toWalletId,
            CashTransactions::VALUE => $amount,
            CashTransactions::STATUS => CashTransactions::STATUS_PENDING
        ]);
    }

    private function subtractFromWallet(int $walletId, float $amount): void {
        $wallet = $this->wallets->findOrFail($walletId);
        $wallet->{Wallets::AMOUNT} -= $amount;
        $wallet->save();
    }

    private function addToWallet(int $walletId, float $amount): void {
        $wallet = $this->wallets->findOrFail($walletId);
        $wallet->{Wallets::AMOUNT} += $amount;
        $wallet->save();
    }

    /**
     * @param string $number
     * @param bool $checkBlocked
     * @return Wallets
     * @throws WalletNotFoundException
     */
    private function findWalletByNumber(string $number, bool $checkBlocked = false): Wallets {
        $wallet = $this->wallets->where(Wallets::WALLET_NUMBER, $number);

        if ($checkBlocked) {
            $wallet = $wallet->where(Wallets::BLOCKED, 0);
        }
        $wallet = $wallet->first();

        if (is_null($wallet)) {
            throw new WalletNotFoundException();
        }

        return  $wallet;
    }

    /**
     * @param int $transactionId
     * @return CashTransactions
     * @throws InvalidTransactionException
     */
    private function findTransaction(int $transactionId): CashTransactions {
        $transaction = $this->transactions
            ->where(CashTransactions::STATUS, CashTransactions::STATUS_PENDING)
            ->where(CashTransactions::ID, $transactionId)
            ->first();

        if (is_null($transaction)) {
            throw new InvalidTransactionException();
        }
        return $transaction;
    }
}
