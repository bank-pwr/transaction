<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTMWalletsTable extends Migration
{
    const TABLE = 'tm_wallets';
    const ID = 'id';
    const USER_ID = 'user_id';
    const WALLET_NUMBER = 'wallet_number';
    const AMOUNT = 'amount';
    const BLOCKED = 'blocked';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(self::TABLE)) {
            Schema::create(self::TABLE, function (Blueprint $table) {
                $table->increments(self::ID);
                $table->integer(self::USER_ID, false, true);
                $table->string(self::WALLET_NUMBER);
                $table->float(self::AMOUNT);
                $table->boolean(self::BLOCKED);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::TABLE);
    }
}
