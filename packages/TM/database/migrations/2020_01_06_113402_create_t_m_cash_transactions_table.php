<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTMCashTransactionsTable extends Migration
{
    const TABLE = 'tm_cash_transactions';
    const ID = 'id';
    const FROM_WALLET_ID = 'from_wallet_id';
    const TO_WALLET_ID = 'to_wallet_id';
    const VALUE = 'value';
    const STATUS = 'status';

    const WALLETS_TABLE = 'tm_wallets';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(self::TABLE)) {
            Schema::create(self::TABLE, function (Blueprint $table) {
                $table->increments(self::ID);
                $table->integer(self::FROM_WALLET_ID, false, true);
                $table->integer(self::TO_WALLET_ID, false, true);
                $table->float(self::VALUE);
                $table->tinyInteger(self::STATUS);
                $table->timestamps();

                $table->foreign(self::FROM_WALLET_ID)
                    ->references(self::ID)
                    ->on(self::WALLETS_TABLE)
                    ->onDelete('cascade');

                $table->foreign(self::TO_WALLET_ID)
                    ->references(self::ID)
                    ->on(self::WALLETS_TABLE)
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::TABLE);
    }
}
