<?php

namespace TM\Tests\Feature;

use Tests\TestCase;
use TM\Models\Wallets;

trait ApiCalls
{
    public function makeApiCall(string $path, int $amount): TestCase {
        $moduleConfig = config('modules');
        $moduleName = array_keys($moduleConfig)[0];

        Wallets::query()->create([
            Wallets::MODULE => $moduleName
        ]);

        $headers = [
            'Module-name' => $moduleName
        ];

        $data = [
            'amount' => $amount
        ];

        return $this->post($path, $data, $headers);
    }
}
