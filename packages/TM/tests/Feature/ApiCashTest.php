<?php

namespace TM\Tests\Unit;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\WithoutMiddleware;
use Tests\Feature\Auth\Traits\PrepareUserBeforeLogin;
use Tests\TestCase;
use TM\Tests\Feature\ApiCalls;

class ApiCashTest extends TestCase
{
    use DatabaseMigrations, PrepareUserBeforeLogin, ApiCalls, WithoutMiddleware;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    public function testApiAddCash() {
        $this->prepareUserBeforeLogin();

        $response = $this->makeApiCall('user/1/add/cash', 10);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(200);
        $this->assertEquals(true, (bool) $responseData['success']);
        $this->assertEquals(110, $responseData['amount']);
    }

    public function testMultipleApiAddCash() {
        $this->prepareUserBeforeLogin();

        $this->makeApiCall('user/1/add/cash', 6);
        $this->makeApiCall('user/1/add/cash', 10);
        $response = $this->makeApiCall('user/1/add/cash', 5);
        $responseData = json_decode($response->response->content(), true);

        $this->assertEquals(121, $responseData['amount']);
    }

    public function testApiRemoveCash() {
        $this->prepareUserBeforeLogin();

        $response = $this->makeApiCall('user/1/remove/cash', 10);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(200);
        $this->assertEquals(true, (bool) $responseData['success']);
        $this->assertEquals(90, $responseData['amount']);
    }

    public function testMultipleApiRemoveCash() {
        $this->prepareUserBeforeLogin();

        $this->makeApiCall('user/1/remove/cash', 1);
        $this->makeApiCall('user/1/remove/cash', 2);
        $response = $this->makeApiCall('user/1/remove/cash', 3);
        $responseData = json_decode($response->response->content(), true);

        $this->assertEquals(94, $responseData['amount']);
    }

    public function testInvalidAmountApiAddCash() {
        $this->prepareUserBeforeLogin();

        $response = $this->makeApiCall('user/1/add/cash', -1);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(403);
        $this->assertEquals(false, (bool) $responseData['success']);
    }

    public function testInvalidAmountApiRemoveCash() {
        $this->prepareUserBeforeLogin();

        $response = $this->makeApiCall('user/1/remove/cash', -1);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(403);
        $this->assertEquals(false, (bool) $responseData['success']);
    }

    public function testFailApiModuleAddCash() {
        $this->prepareUserBeforeLogin();

        $response = $this->post('user/1/add/cash', ['amount' => 10]);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(404);
        $this->assertEquals(false, (bool) $responseData['success']);
    }

    public function testFailApiModuleRemoveCash() {
        $this->prepareUserBeforeLogin();

        $response = $this->post('user/1/remove/cash', ['amount' => 10]);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(404);
        $this->assertEquals(false, (bool) $responseData['success']);
    }

    public function testFailApiUserAddCash() {
        $this->prepareUserBeforeLogin();

        $response = $this->makeApiCall('user/2/add/cash', 1);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(404);
        $this->assertEquals(false, (bool) $responseData['success']);
    }

    public function testFailApiUserRemoveCash() {
        $this->prepareUserBeforeLogin();

        $response = $this->makeApiCall('user/2/add/cash', 1);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(404);
        $this->assertEquals(false, (bool) $responseData['success']);
    }

    public function testApiRemoveTooMuchCash() {
        $this->prepareUserBeforeLogin();

        $response = $this->makeApiCall('user/1/remove/cash', 110);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(403);
        $this->assertEquals(false, (bool) $responseData['success']);
    }
}
