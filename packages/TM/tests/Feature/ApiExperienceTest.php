<?php

namespace TM\Tests\Unit;

use App\Models\Levels;
use App\Models\User;
use Illuminate\Database\Eloquent\Factory;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\WithoutMiddleware;
use Tests\Feature\Auth\Traits\PrepareUserBeforeLogin;
use Tests\TestCase;
use TM\Tests\Feature\ApiCalls;

class ApiExperienceTest extends TestCase
{
    use DatabaseMigrations, PrepareUserBeforeLogin, ApiCalls, WithoutMiddleware;

    protected Factory $baseFactory;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $pathToBaseFactories = base_path().'/database/factories';
        $this->baseFactory = Factory::construct(\Faker\Factory::create(), $pathToBaseFactories);
    }

    public function testApiAddExperience() {
        $this->prepareUserBeforeLogin();
        $this->prepareLevels();

        $response = $this->makeApiCall('user/1/add/experience', 10);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(200);
        $this->assertEquals(true, (bool) $responseData['success']);
        $this->assertEquals(10, $responseData['amount']);
    }

    public function testMultipleApiAddExperience() {
        $this->prepareUserBeforeLogin();
        $this->prepareLevels();

        $this->makeApiCall('user/1/add/experience', 6);
        $this->makeApiCall('user/1/add/experience', 10);
        $response = $this->makeApiCall('user/1/add/experience', 5);
        $responseData = json_decode($response->response->content(), true);

        $this->assertEquals(21, $responseData['amount']);
    }

    public function testInvalidAmountApiAddExperience() {
        $this->prepareUserBeforeLogin();
        $this->prepareLevels();

        $response = $this->makeApiCall('user/1/add/experience', -1);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(403);
        $this->assertEquals(false, (bool) $responseData['success']);
    }

    public function testFailApiModuleAddExperience() {
        $this->prepareUserBeforeLogin();
        $this->prepareLevels();

        $response = $this->post('user/1/add/experience', ['amount' => 10]);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(404);
        $this->assertEquals(false, (bool) $responseData['success']);
    }

    public function testFailApiUserAddExperience() {
        $this->prepareUserBeforeLogin();
        $this->prepareLevels();

        $response = $this->makeApiCall('user/2/add/experience', 1);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(404);
        $this->assertEquals(false, (bool) $responseData['success']);
    }

    public function testApiLevelUp() {
        $this->prepareUserBeforeLogin();
        $this->prepareLevels(2);

        $response = $this->makeApiCall('user/1/add/experience', 41);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(200);
        $this->assertEquals(1, $responseData['amount']);
    }

    public function testApiThresholdReached() {
        $this->prepareUserBeforeLogin();
        $this->prepareLevels(2);

        $response = $this->makeApiCall('user/1/add/experience', 999);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(500);
        $this->assertEquals(false, (bool) $responseData['success']);
    }

    protected function prepareLevels(int $ammount = 1) {
        $this->baseFactory->of(Levels::class)->times($ammount)->create();
        $levels = Levels::all();

        foreach ($levels as $level) {
            $level->{Levels::LEVEL} = $level->{Levels::ID};
            $level->save();
        }
    }
}
