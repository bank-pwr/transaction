<?php

namespace TM\Tests\Unit;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\WithoutMiddleware;
use Tests\Feature\Auth\Traits\PrepareUserBeforeLogin;
use Tests\TestCase;
use TM\Tests\Feature\ApiCalls;

class ApiVipCashTest extends TestCase
{
    use DatabaseMigrations, PrepareUserBeforeLogin, ApiCalls, WithoutMiddleware;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    public function testApiAddVipCash() {
        $this->prepareUserBeforeLogin();

        $response = $this->makeApiCall('user/1/add/vip-cash', 10);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(200);
        $this->assertEquals(true, (bool) $responseData['success']);
        $this->assertEquals(10, $responseData['amount']);
    }

    public function testMultipleApiAddVipCash() {
        $this->prepareUserBeforeLogin();

        $this->makeApiCall('user/1/add/vip-cash', 6);
        $this->makeApiCall('user/1/add/vip-cash', 10);
        $response = $this->makeApiCall('user/1/add/vip-cash', 5);
        $responseData = json_decode($response->response->content(), true);

        $this->assertEquals(21, $responseData['amount']);
    }

    public function testApiRemoveVipCash() {
        $this->prepareUserBeforeLogin();
        $this->makeApiCall('user/1/add/vip-cash', 100);

        $response = $this->makeApiCall('user/1/remove/vip-cash', 10);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(200);
        $this->assertEquals(true, (bool) $responseData['success']);
        $this->assertEquals(90, $responseData['amount']);
    }

    public function testMultipleApiRemoveVipCash() {
        $this->prepareUserBeforeLogin();
        $this->makeApiCall('user/1/add/vip-cash', 100);

        $this->makeApiCall('user/1/remove/vip-cash', 1);
        $this->makeApiCall('user/1/remove/vip-cash', 2);
        $response = $this->makeApiCall('user/1/remove/vip-cash', 3);
        $responseData = json_decode($response->response->content(), true);

        $this->assertEquals(94, $responseData['amount']);
    }

    public function testInvalidAmountApiAddVipCash() {
        $this->prepareUserBeforeLogin();

        $response = $this->makeApiCall('user/1/add/vip-cash', -1);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(403);
        $this->assertEquals(false, (bool) $responseData['success']);
    }

    public function testInvalidAmountApiRemoveVipCash() {
        $this->prepareUserBeforeLogin();

        $response = $this->makeApiCall('user/1/remove/vip-cash', -1);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(403);
        $this->assertEquals(false, (bool) $responseData['success']);
    }

    public function testFailApiModuleAddVipCash() {
        $this->prepareUserBeforeLogin();

        $response = $this->post('user/1/add/vip-cash', ['amount' => 10]);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(404);
        $this->assertEquals(false, (bool) $responseData['success']);
    }

    public function testFailApiModuleRemoveVipCash() {
        $this->prepareUserBeforeLogin();

        $response = $this->post('user/1/remove/vip-cash', ['amount' => 10]);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(404);
        $this->assertEquals(false, (bool) $responseData['success']);
    }

    public function testFailApiUserAddVipCash() {
        $this->prepareUserBeforeLogin();

        $response = $this->makeApiCall('user/2/add/vip-cash', 1);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(404);
        $this->assertEquals(false, (bool) $responseData['success']);
    }

    public function testFailApiUserRemoveVipCash() {
        $this->prepareUserBeforeLogin();

        $response = $this->makeApiCall('user/2/add/vip-cash', 1);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(404);
        $this->assertEquals(false, (bool) $responseData['success']);
    }

    public function testApiRemoveTooMuchVipCash() {
        $this->prepareUserBeforeLogin();

        $response = $this->makeApiCall('user/1/remove/vip-cash', 110);
        $responseData = json_decode($response->response->content(), true);

        $response->assertResponseStatus(403);
        $this->assertEquals(false, (bool) $responseData['success']);
    }
}
