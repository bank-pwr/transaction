<?php

namespace TM\Tests\Unit;

use App\Exceptions\LevelOutOfRangeException;
use App\Models\Levels;
use App\Models\User;
use Illuminate\Database\Eloquent\Factory;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Tests\Feature\Auth\Traits\PrepareUserBeforeLogin;
use Tests\TestCase;
use TM\Exceptions\InvalidAmountException;
use TM\Exceptions\InvalidValueException;
use TM\Facades\TM;
use TM\Models\CashTransactions;
use TM\Models\Wallets;

class TMCashTest extends TestCase
{
    use DatabaseMigrations, PrepareUserBeforeLogin;

    protected Factory $moduleFactory;
    protected Factory $baseFactory;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $pathToModuleFactories = base_path().'/packages/TM/database/factories';
        $pathToBaseFactories = base_path().'/database/factories';
        $this->moduleFactory = Factory::construct(\Faker\Factory::create(), $pathToModuleFactories);
        $this->baseFactory = Factory::construct(\Faker\Factory::create(), $pathToBaseFactories);
    }

    public function testAddCash() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();

        $cash = TM::user(1)->module($module->{Wallets::MODULE})->addCash(250);
        $this->assertEquals(350, $cash);
    }

    public function testMultipleAddCash() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();

        TM::user(1)->module($module->{Wallets::MODULE})->addCash(250);
        TM::user(1)->module($module->{Wallets::MODULE})->addCash(100);
        $cash = TM::user(1)->module($module->{Wallets::MODULE})->addCash(10);
        $this->assertEquals(460, $cash);
    }

    public function testCashRows() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();

        TM::user(1)->module($module->{Wallets::MODULE})->addCash(250);
        TM::user(1)->module($module->{Wallets::MODULE})->addCash(100);
        TM::user(1)->module($module->{Wallets::MODULE})->addCash(10);

        $rows = CashTransactions::all()->count();
        $this->assertEquals(3, $rows);
    }

    public function testRemoveCash() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();

        $cash = TM::user(1)->module($module->{Wallets::MODULE})->removeCash(10);

        $this->assertEquals(90, $cash);
    }

    public function testInvalidValueException() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();

        $this->expectException(InvalidValueException::class);
        TM::user(1)->module($module->{Wallets::MODULE})->removeCash(110);
    }

    public function testInvalidAmountException() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();

        $this->expectException(InvalidAmountException::class);
        TM::user(1)->module($module->{Wallets::MODULE})->removeCash(-1);
    }

}
