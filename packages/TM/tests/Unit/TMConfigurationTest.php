<?php

namespace TM\Tests\Unit;

use Illuminate\Database\Eloquent\Factory;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Tests\Feature\Auth\Traits\PrepareUserBeforeLogin;
use Tests\TestCase;
use TM\Exceptions\ModuleNotFoundException;
use TM\Exceptions\UserNotFoundException;
use TM\Facades\TM;
use TM\Models\Wallets;

class TMConfigurationTest extends TestCase
{
    use DatabaseMigrations, PrepareUserBeforeLogin;

    protected Factory $moduleFactory;
    protected Factory $baseFactory;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $pathToModuleFactories = base_path().'/packages/TM/database/factories';
        $pathToBaseFactories = base_path().'/database/factories';
        $this->moduleFactory = Factory::construct(\Faker\Factory::create(), $pathToModuleFactories);
        $this->baseFactory = Factory::construct(\Faker\Factory::create(), $pathToBaseFactories);
    }

    public function testModuleException() {
        $this->prepareUserBeforeLogin();

        $this->expectException(ModuleNotFoundException::class);
        TM::user(1)->addExperience(1);
    }

    public function testUserException() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();

        $this->expectException(UserNotFoundException::class);
        TM::user(2)->module($module->{Wallets::MODULE})->addExperience(1);
    }
}
