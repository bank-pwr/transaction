<?php

namespace TM\Tests\Unit;

use App\Exceptions\LevelOutOfRangeException;
use App\Models\Levels;
use App\Models\User;
use Illuminate\Database\Eloquent\Factory;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Tests\Feature\Auth\Traits\PrepareUserBeforeLogin;
use Tests\TestCase;
use TM\Exceptions\InvalidAmountException;
use TM\Facades\TM;
use TM\Models\ExpTransactions;
use TM\Models\Wallets;

class TMExperienceTest extends TestCase
{
    use DatabaseMigrations, PrepareUserBeforeLogin;

    protected Factory $moduleFactory;
    protected Factory $baseFactory;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $pathToModuleFactories = base_path().'/packages/TM/database/factories';
        $pathToBaseFactories = base_path().'/database/factories';
        $this->moduleFactory = Factory::construct(\Faker\Factory::create(), $pathToModuleFactories);
        $this->baseFactory = Factory::construct(\Faker\Factory::create(), $pathToBaseFactories);
    }

    public function testAddExperience() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();
        $this->prepareLevels();

        $experience = TM::user(1)->module($module->{Wallets::MODULE})->addExperience(5);
        $this->assertEquals(5, $experience);
    }

    public function testHistoryRowCreated() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();
        $this->prepareLevels();

        TM::user(1)->module($module->{Wallets::MODULE})->addExperience(5);
        $transactions = ExpTransactions::all()->count();

        $this->assertEquals(1, $transactions);
    }

    public function testLevelUpdate() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();
        $this->prepareLevels(5);

        TM::user(1)->module($module->{Wallets::MODULE})->addExperience(120);
        $user = User::first();

        $this->assertEquals(4, $user->{User::LEVEL});
    }

    public function testThresholdException() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();

        $this->expectException(LevelOutOfRangeException::class);

        TM::user(1)->module($module->{Wallets::MODULE})->addExperience(1);
    }

    public function testAmountException() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();

        $this->expectException(InvalidAmountException::class);

        TM::user(1)->module($module->{Wallets::MODULE})->addExperience(-1);
    }

    protected function prepareLevels(int $ammount = 1) {
        $this->baseFactory->of(Levels::class)->times($ammount)->create();
        $levels = Levels::all();

        foreach ($levels as $level) {
            $level->{Levels::LEVEL} = $level->{Levels::ID};
            $level->save();
        }
    }
}
