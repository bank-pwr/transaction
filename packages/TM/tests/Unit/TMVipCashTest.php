<?php

namespace TM\Tests\Unit;

use Illuminate\Database\Eloquent\Factory;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Tests\Feature\Auth\Traits\PrepareUserBeforeLogin;
use Tests\TestCase;
use TM\Exceptions\InvalidAmountException;
use TM\Exceptions\InvalidValueException;
use TM\Facades\TM;
use TM\Models\Wallets;
use TM\Models\VipCashTransactions;

class TMVipCashTest extends TestCase
{
    use DatabaseMigrations, PrepareUserBeforeLogin;

    protected Factory $moduleFactory;
    protected Factory $baseFactory;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $pathToModuleFactories = base_path().'/packages/TM/database/factories';
        $pathToBaseFactories = base_path().'/database/factories';
        $this->moduleFactory = Factory::construct(\Faker\Factory::create(), $pathToModuleFactories);
        $this->baseFactory = Factory::construct(\Faker\Factory::create(), $pathToBaseFactories);
    }

    public function testAddVipCash() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();

        $vipCash = TM::user(1)->module($module->{Wallets::MODULE})->addVipCash(250);
        $this->assertEquals(250, $vipCash);
    }

    public function testMultipleAddVipCash() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();

        TM::user(1)->module($module->{Wallets::MODULE})->addVipCash(250);
        TM::user(1)->module($module->{Wallets::MODULE})->addVipCash(100);
        $vipCash = TM::user(1)->module($module->{Wallets::MODULE})->addVipCash(10);
        $this->assertEquals(360, $vipCash);
    }

    public function testVipCashRows() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();

        TM::user(1)->module($module->{Wallets::MODULE})->addVipCash(250);
        TM::user(1)->module($module->{Wallets::MODULE})->addVipCash(100);
        TM::user(1)->module($module->{Wallets::MODULE})->addVipCash(10);

        $rows = VipCashTransactions::all()->count();
        $this->assertEquals(3, $rows);
    }

    public function testRemoveVipCash() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();

        TM::user(1)->module($module->{Wallets::MODULE})->addVipCash(100);
        $vipCash = TM::user(1)->module($module->{Wallets::MODULE})->removeVipCash(10);

        $this->assertEquals(90, $vipCash);
    }

    public function testInvalidValueException() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();

        $this->expectException(InvalidValueException::class);
        TM::user(1)->module($module->{Wallets::MODULE})->removeVipCash(110);
    }

    public function testInvalidAmountException() {
        $this->prepareUserBeforeLogin();
        $module = $this->moduleFactory->of(Wallets::class)->create();

        $this->expectException(InvalidAmountException::class);
        TM::user(1)->module($module->{Wallets::MODULE})->removeVipCash(-1);
    }
}
