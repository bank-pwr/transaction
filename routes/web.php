<?php
$router->post('create-wallet', 'WalletController@createWallet');

$router->group(['middleware' => ['service-user']], function() use ($router) {
    $router->get('user-info', 'WalletController@getUserInfo');
    $router->post('lock-wallet', 'WalletController@lockWallet');
    $router->post('unlock-wallet', 'WalletController@unlockWallet');
    $router->get('user-transactions', 'WalletController@getUserTransactions');
    $router->get('get-wallets', 'WalletController@getWallets');

    $router->group(['prefix' => 'transactions'], function () use ($router) {
        $router->get('all', 'TMController@getAllTransactions');
        $router->post('make', 'TMController@makeTransaction');
        $router->post('accept', 'TMController@acceptTransaction');
        $router->post('rollback', 'TMController@rollbackTransaction');
    });
});

$router->get('test', function() {
    return 'test';
});
